﻿using Novell.Directory.Ldap;
using System;

namespace ADAuthConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {

            do
            {

                Console.Write("host: ");
                string host = Console.ReadLine();

                Console.Write("user: ");
                string user = Console.ReadLine();

                Console.Write("pwd: ");
                string pwd = Console.ReadLine();
                int port = 389;

                var connection = new LdapConnection();
                try
                {
                    connection.Connect(host, port);
                    connection.Bind(LdapConnection.Ldap_V3, user, pwd);
                    Console.WriteLine("Sucesso!");
                }
                catch (LdapException ldapError)
                {
                    Console.WriteLine("#ERROR: ");
                    Console.WriteLine(ldapError);
                }
                catch (Exception error)
                {
                    Console.WriteLine("#ERROR: ");
                    Console.WriteLine(error);
                }

                Console.WriteLine("");
                Console.WriteLine("");

            } while (true);
        }
    }
}
